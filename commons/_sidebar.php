<aside>
	<img src="" alt="Bitamina Digital" class="logo">
	<div class="nav">
		<li>
			<i class="fas fa-th-large"></i>
			Dashboard
		</li>
		<li>
			<i class="fas fa-users"></i>
			Users
		</li>
		<li>
			<i class="fas fa-bullseye"></i>
			Leads
		</li>
	</div>
	<span class="signOut">
		<i class="fas fa-sign-out-alt"></i>
	</span>
</aside>